import { useState, useEffect } from "react"

import { Card, Button } from "react-bootstrap"
import { Link } from "react-router-dom"

//destructure the courseProp from thje prop parameter
//CourseCard(prop)
export default function CourseCard({courseProp}){

	// console.log(props.courseProp.name);
	// console.log(typeof props);
	// console.log(courseProp)

	// Scenarion Keep tack the number of enrolees of each course.

	const { _id, name,description,price, slots } = courseProp;

	// Syntax of useState:
		// const [stateName , setStatement] = useState(initialStateValue);
		// Using the state hook, it returns an array with the following elements:
			// first element contains the current initial State value.
			//second element is a function that is used to change the value of the first element
		// const [count, setCount] = useState(0);
		
		// const [seats, setSeats] = useState(30);
		// Function that keeps track of the enrollees for a course

		/*
			We will refactor the "enroll" function using the "useEffect" hooks to disable the enroll button when the seats reaches zero.
		*/
		// Use the disabled state to disable the enroll button
		// const [disabled, setDisabled] = useState(false);

		//Syntax: useEffect(function, [dependency])

		// No dependency array passed
		// If the useEffect() doest not have a depedency array, it will run on initial render and whenever a state is set by its function.

		// useEffect(()=>{
		// 	console.log("will run on initial render or on every changes with our components");
		// })

		// an empty array
			// Ifthe useEffect() has dependency array but it is empty, it will only run on the initial render.
		// useEffect(() => {
		// 	console.log("will only run on initial render")
		// }, [])

		// with dependency array(props or state values);
			// if the useEffect() has a dependency array and there is state or data in it, the useEffect() will run whenever that state is updated
		// useEffect(() => {
		// 	console.log("will run on initial render and every change on the dependency value.")
		// }, [seats, count]);

		// function unEnroll(){
		// 	setCount(count -1)
		// }

		// function enroll(){
		// 	//Activity Solution
		// 	// if (seats > 0){
		// 	// 	setCount(count+1);
		// 	// 	console.log(`Enrolees: ${count}`)
		// 	// 	setSeats(seats - 1)
		// 	// 	console.log(`Seats: ${seats}`)
		// 	// } else {
		// 	// 	alert('No more seats available')
		// 	// }

		// 	setCount(count+1);
		// 	console.log(`Enrolees: ${count}`)
		// 	setSeats(seats - 1)
		// 	console.log(`Seats: ${seats}`)
		// }

		// useEffect(() => {
		// 	if(seats <= 0){
		// 		setDisabled(true);
		// 		alert("No more seats available");
		// 	}
		// }, [seats])

	return(
		<Card className="my-3">
		      <Card.Body>
		        <Card.Title>
		        {name}
		        </Card.Title>
		        <Card.Subtitle>
		      Description:
		      </Card.Subtitle>
		        <Card.Text>
		         {description}
		        </Card.Text>
		        <Card.Subtitle>
		        Price:
		        </Card.Subtitle>
		        <Card.Text>
		          {price}
		        </Card.Text>
		        <Card.Subtitle>
		        	Slots:
		        </Card.Subtitle>
		        <Card.Text>
		          {slots} available
		        </Card.Text>
		        <Button variant="primary" as={Link} to={`/courses/${_id}`}>Details</Button>
		      </Card.Body>
		    </Card>
	)
}