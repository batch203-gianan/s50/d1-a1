
// We will use React's context API to give the login user object to have "global" scope within our application.

import React from "react";

// Create a COntext Object
// A context object with object data type that can be used to store information that can be shared to other components within the app
const UserContext = React.createContext();

// The "Provider" component allows other components to consume/user the context object and supply the necessary information needed in the context object.
export const UserProvider = UserContext.Provider;

export default UserContext;