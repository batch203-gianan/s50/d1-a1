import { useState, useEffect } from "react";

import { BrowserRouter as Router, Routes, Route} from "react-router-dom";
import { Container } from "react-bootstrap";
import AppNavbar from "./components/AppNavbar";

import Home from "./pages/Home";
import Courses from "./pages/Courses";
import CourseView from "./pages/CourseView";
import Register from "./pages/Register"
import Logout from "./pages/Logout"
import Login from "./pages/Login"
import Error from "./pages/Error"
import './App.css';
import { UserProvider } from "./UserContext";


/*
 All other components/pages will be contained in our main (parent) component: App

 We use React fragment (<>...</>) to render adjacent elements
*/



function App() {

  // Create a "User" state and set an "unsetUser" function that will be used in different pages/components within the application

  // Global state hooks for the user information for validating if a user is logged in.
  const [user, setUser] = useState({
    // email: localStorage.getItem("email");
    id: null,
    isAdmin: null
  });

  // Function for clearing localStorage
  const unsetUser = () => {
    // Allow us to clear the information in the localStorage
    localStorage.clear();
  }

  useEffect(() => {
    console.log(user);
    console.log(localStorage);
  }, [user])

  // To update a User state upon a page load is initiated and a user already exists

  useEffect(() =>{
      fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
          headers: {
              Authorization: `Bearer ${localStorage.getItem("token")}`
          }
      })
      .then(res => res.json())
      .then(data => {
          console.log(data);

          // Set the user states value if the token already exists in the local storage
          if(data._id !== undefined){
            setUser({
                id: data._id,
                isAdmin: data.isAdmin
            });
          }
          // set back to the initial state of the user if no token found in the local storage
          else{
            setUser({
                id: null,
                isAdmin: null
            });
          }
      })

  },[])
  
  return (
    // All information in the context by providing the information using the "UserProvider" component and passing the information via the "value" prop.
    // All the information inside the value prop will be accessible to pages/components wrapped around with the UserProvider
    <UserProvider value={{user, setUser, unsetUser}}>
   {/* Router component is used to wrapped around all components which will have access to the routing system*/}
    <Router>
    <AppNavbar />
    <Container>
      {/* Routes holds all our Route components.*/}
      <Routes>
        {/*
          Route assign an endpoint and display the appropriate page component for that endpoint.
          -"exact" and path "props" to assign the endpoint and the page should be only accessed
        */}
        <Route exact path="/" element={<Home/ >} />
        <Route exact path="/courses" element={<Courses/ >} />
        <Route exact path="/courses/:courseId" element={<CourseView/ >} />
        <Route exact path="/login" element={<Login/ >} />
        <Route exact path="/logout" element={<Logout/ >} />
        <Route exact path="/register" element={<Register/ >} />
        <Route exact path="*" element={<Error/ >} />
      </Routes>

    </Container>
    </Router>
    </UserProvider>
  );
}

export default App;

