import { useState, useEffect } from "react";

import {Container, Card, Button, Row, Col} from "react-bootstrap";

import { useParams, useNavigate } from "react-router-dom";

import Swal from "sweetalert2"

export default function CourseView(){

    // "useParams" hooks allows us to retrieve the courseId passed via the URL.

    const { courseId } = useParams();

    const navigate = useNavigate();

	// Create state hooks to capture the information of a specific course and display it in our application.

	const [name, setName] = useState('')
	const [description, setDescription] = useState('')
	const [price, setPrice] = useState(0);

    useEffect(() =>{
        console.log(courseId);

        fetch(`${ process.env.REACT_APP_API_URL }/courses/${courseId}`)
        .then(res => res.json())
        .then(data => {

            console.log(data);

            setName(data.name);
            setDescription(data.description);
            setPrice(data.price);

        });

    }, [courseId])

    const enroll = (courseId) => {

        fetch(`${ process.env.REACT_APP_API_URL }/users/enroll`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                Authorization: `Bearer ${ localStorage.getItem('token') }`
            },
            body: JSON.stringify({
                courseId: courseId
            })
        })
        .then(res => res.json())
        .then(data => {

            console.log(data);

            if(data){
                Swal.fire({
                    title: "Successfully Enrolled",
                    icon: "success",
                    text: "You have successfully enrolled for this course"
                });

                navigate("/courses");
            }
            else{
                Swal.fire({
                    title: "Something went wrong",
                    icon: "error",
                    text: "Please try again."
                });
            }

        });
    }

    return(
        <Container className="mt-5">
            <Row>
                <Col lg={{ span: 6, offset: 3 }}>
                    <Card>
                        <Card.Body className="text-center">
                            <Card.Title>{name}</Card.Title>
                            <Card.Subtitle>Description:</Card.Subtitle>
                            <Card.Text>{description}</Card.Text>
                            <Card.Subtitle>Price:</Card.Subtitle>
                            <Card.Text>PhP {price}</Card.Text>
                            <Card.Subtitle>Class Schedule</Card.Subtitle>
                            <Card.Text>8 am - 5 pm</Card.Text>
                            <Button variant="primary"  size="lg" onClick={() => enroll(courseId)}>Enroll</Button>
                        </Card.Body>        
                    </Card>
                </Col>
            </Row>
        </Container>
    )
}