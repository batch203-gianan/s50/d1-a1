import { useState, useEffect } from "react";

import { useContext } from "react";

import { Navigate, useNavigate } from "react-router-dom";

import { Form, Button } from "react-bootstrap";

import Swal from "sweetalert2";

import UserContext from "../UserContext";

export default function Register(){
    // create a statehooks to sture the values of the input fields

    const { user } = useContext(UserContext);

    const navigate = useNavigate();

    const [fName, setFName] = useState('');
    const [lName, setLName] = useState('');
    const [email, setEmail] = useState('');
    const [mobileNo, setMobileNo] = useState('');
    const [password1, setPassword1]= useState('');
    const [password2, setPassword2]= useState('');

    // Create a state to determin whether the submit button is disabled or not.
    const [isActive, setIsActive] = useState(false);

    /*
		Two way binding
			-is done so that we can assure that we can save the input into our states as we type into the input elements. This is so we don't ave to save it just before submit.

			e.target = current element where the event happened
    */

    // Check if the  values are successfully binded
    	console.log(fName);
    	console.log(lName);
    	console.log(email);
    	console.log(mobileNo);
    	console.log(password1);
    	console.log(password2);

    	useEffect(() =>{

    		//Enable the submit button if:
    			// All the fields are populated
    			// both passwords match.
    		if((fName !== '' && lName !=='' && email !== '' && mobileNo !=='' && password1 !== '' && password2 !== '') && (password1=== password2)){
    			setIsActive(true);
    		} else{
    			setIsActive(false);
    		}

    	}, [fName, lName, email, mobileNo, password1, password2])

    	// Function to simulate user registration
    	function registerUser(e){
    		// prevents page loading/redirection via form submission
    		e.preventDefault();

            fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`,{
                method: "POST",
                headers: {
                    "Content-Type": "application/json"
                },
                    body: JSON.stringify({
                    email: email
                    })
                })
                .then(res => res.json())
                .then(data => {
                    console.log(data)
                    if(data){
                        Swal.fire({
                            title: "Duplicate email found",
                            icon: "error",
                            text: "kidnly provide another email to complete the registration"
                        })
                    }
                    else{
                        fetch(`${process.env.REACT_APP_API_URL}/users/register `, {
                            method: "POST",
                            headers: {
                                "Content-Type": "application/json"
                            },
                            body: JSON.stringify({
                                firstName: fName,
                                lastName: lName,
                                email: email,
                                password: password1,
                                mobileNumber: mobileNo
                            })
                        })
                        .then(res => res.json())
                        .then(data => {
                            console.log(data);

                            if(data){
                                Swal.fire({
                                    title: "Registration Successful",
                                    icon: "success",
                                    text: "Welcome to Zuitt!"
                                });

                                //clear input fields
                                setFName('');
                                setLName('');
                                setEmail('');
                                setMobileNo('');
                                setPassword1('');
                                setPassword2('');

                                // Allow us to redirect the user to the login page after account registration

                                navigate("/login");

                            }
                            else{
                                Swal.fire({
                                    title: "Something went wrong",
                                    icon: "error",
                                    text: "Please try again."
                                });
                            }
                        })
                    }
                })
    	}

    return(
        // (user.id !== null)
        // ?
        //     <Navigate to="/courses" />
        // :
    	<>
    		<h1 className="my-5 text-center">Register</h1>
    	    <Form onSubmit={e => registerUser(e)}>

    	      <Form.Group className="mb-3" controlId="firstName">
    	        <Form.Label>First Name</Form.Label>
    	        <Form.Control 
    	        type="text" 
    	        placeholder="Enter first name"
    	        onChange={e => setFName(e.target.value)}
    	        value={fName}
    	        required />
    	      </Form.Group>

    	      <Form.Group className="mb-3" controlId="lastName">
    	        <Form.Label>Last Name</Form.Label>
    	        <Form.Control 
    	        type="text" 
    	        placeholder="Enter last name"
    	        onChange ={e=> setLName(e.target.value)} 
    	        value ={lName}
    	        required/>
    	      </Form.Group>

    	      <Form.Group className="mb-3" controlId="emailAddress">
    	        <Form.Label>Email address</Form.Label>
    	        <Form.Control 
    	        type="email" 
    	        placeholder="Enter email" 
    	        onChange ={e=> setEmail(e.target.value)}
    	        value ={email}
    	        required/>
    	        <Form.Text className="text-muted">
    	          We'll never share your email with anyone else.
    	        </Form.Text>
    	      </Form.Group>

    	      <Form.Group className="mb-3" controlId="mobileNo">
    	        <Form.Label>Mobile Number</Form.Label>
    	        <Form.Control 
    	        type="number" 
    	        placeholder="09xxxxxxxxxxx"
    	        onChange ={e=> setMobileNo(e.target.value)}
    	        value ={mobileNo}
    	        required/>
    	      </Form.Group>

    	      <Form.Group className="mb-3" controlId="password1">
    	        <Form.Label>Password</Form.Label>
    	        <Form.Control type="password" 
    	        placeholder="Enter Password" 
    	        onChange ={e=> setPassword1(e.target.value)}
    	        value={password1}
    	        required/>
    	        
    	      </Form.Group>

    	      <Form.Group className="mb-3" controlId="password2">
    	        <Form.Label>Verify Password</Form.Label>
    	        <Form.Control type="password" 
    	        placeholder="Verify Password"
    	        onChange ={e=> setPassword2(e.target.value)}
    	        value={password2}
    	        required/>
    	      </Form.Group>
    	      {/*COnditional rendering - submit button will be active based on the isActive state*/}
    	      {
    	      	isActive
    	      	?
	    	      	<Button variant="primary" type="submit" id="submitBtn">
	    	      	  Submit
	    	      	</Button>
	    	     :

	    	     	<Button variant="primary" type="submit" id="submitBtn" disabled>
	    	     	  Submit
	    	     	</Button>
    	      }

    	    </Form>	
    	</>
    )
}