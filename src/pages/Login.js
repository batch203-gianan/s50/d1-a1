import { useState, useEffect, useContext } from "react";

import { Navigate } from "react-router-dom"
import { Form, Button } from "react-bootstrap";
import Swal from "sweetalert2"
import UserContext from "../UserContext";

export default function Login(){

    // Allow us to consume the User Contet object and its values(properties) to use for user validation
    const { user, setUser } = useContext(UserContext);

	//Set the initial state of the form to an empty string

    const [email, setEmail] = useState('');
    const [password, setPassword]= useState('');

    // Set the initial state of the button to false (can't submit if the all fields are empty)

    const [isActive, setIsActive] = useState(false);

    

    // to check if this is binded
    	console.log(email);
    	console.log(password);

    	// We use the hook 'useEffect()' to render something somethign to the button

    // Allow us to gain access to methods that llow us to redirect ot another page.
    //const navigate = useNavigate();

    	useEffect(() =>{

    		if(email !== '' && password !== ''){
    			setIsActive(true);
    		} else{
    			setIsActive(false);
    		}

    	}, [email,password,])

    	// for onChange

    	function authenticate(e){

            //Page redirection
            e.preventDefault();

            /*
                Syntax:
                fetch("url", {options})
                .then(res => res.json()) // convert data
                .then(data => {})

                Grouping:
                /users
                /courses
            */

            fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
                method: "POST",
                headers: {
                    "Content-Type": "application/json"
                },
                body: JSON.stringify({
                    email: email,
                    password: password
                })
            })
            .then(res => res.json())
            .then(data => {
                console.log(data.accessToken);

                if(data.accessToken !== undefined){
                    localStorage.setItem("token", data.accessToken);
                    retrieveUserDetails(data.accessToken);

                    Swal.fire({
                        title: "Login Successful",
                        icon: "success",
                        text: "Welcome to Zuitt!"
                    });
                }
                else{
                    Swal.fire({
                        title: "Authentication Failed",
                        icon: "error",
                        text: "Check your login details and try again."
                    });
                }
            });



    		//"localStorage" is a property that allows JavaScript sites and application to save key-value pairs in a web browser with no expiration date.
    		// Syntax:
    			// localStorage.setItem("propertyName", value);

    		// localStorage.setItem("email", email)

            // setUser({
            //     email: localStorage.getItem("email")
            // });

    		setEmail('');
    		setPassword('');

    		//alert('You are now logged in!')

    		// Redirect us to home page
    		//navigate("/");
    	}

        const retrieveUserDetails = (token) => {
            // Token will be sent as part of the request's header.

            fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
                headers: {
                    Authorization: `Bearer ${token}`
                }
            })
            .then(res => res.json())
            .then(data => {
                console.log(data);

                // Change the global "user" state to store the "id" and "isAdmin" property.
                setUser({
                    id: data._id,
                    isAdmin: data.isAdmin
                });
            })
        }

    return(
        // Create a conditional statement that will redirect the user to the course page when a user is already logged in.
        (user.id !== null)
        ?
            <Navigate to="/courses" />
        :
                <>
                    <h1 className="my-5 text-center">Login</h1>
                    <Form onSubmit={e => authenticate(e)}>
                      <Form.Group className="mb-3" controlId="emailAddress">
                        <Form.Label>Email address</Form.Label>
                        <Form.Control 
                        type="email" 
                        placeholder="Enter email" 
                        onChange ={e=> setEmail(e.target.value)}
                        value ={email}
                        required/>
                      </Form.Group>

                      <Form.Group className="mb-3" controlId="password1">
                        <Form.Label>Password</Form.Label>
                        <Form.Control type="password" 
                        placeholder="Enter Password" 
                        onChange ={e=> setPassword(e.target.value)}
                        value={password}
                        required/>              
                      </Form.Group>

                      {
                        isActive
                        ?
                            <Button variant="success" type="submit" id="submitBtn">
                              Submit
                            </Button>
                         :

                            <Button variant="dark" type="submit" id="submitBtn" disabled>
                              Submit
                            </Button>
                      }

                    </Form> 
                </>
    )
}