import { useEffect, useState } from "react";

import CourseCard from "../components/CourseCard"

// import coursesData from "../data/coursesData";

export default function Course(){

	// course state that will be used to store the courses retrieved in the database.

	const [courses, setCourses] = useState([])

	useEffect(() => {
			// will retrieve all the active courses
			fetch(`${process.env.REACT_APP_API_URL}/courses/`)
			.then(res => res.json())
			.then(data => {
				console.log(data)
				setCourses(
				data.map(course => {
					return(
					<CourseCard key= {course._id} courseProp={course}/>);
				})
				);
			})
	}, []);

	//To confirm if we have imported our mock database
	//console.log(coursesData)

	//console.log(coursesData[0])

	// the curly brace {} are used for props to signify that we are providing information using a JavaScript expressions.

	//Everytime the map method loops through the data, it creates "CourseCard" component and then passes the current element in our coursesData array using courseProp
	// Multiple components are created through the map method must have a unique key that will help ReactJS identify which components/elemetns have been changed, added or removed.
	// const courses = coursesData.map(course => {
	// 	return(
	// 	<CourseCard key= {course.id} courseProp={course}/>
	// 	);
	// })

	return(
		<>
		<h1>Course</h1>
		{courses}
		</>		
	)
}